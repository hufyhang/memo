## Web Service 2.0
### *In Memory of What Happened on the Web 20 Years Ago*

##### *Feifei Hang, 21 Jan, 2014*

Back in the early 1990s, graphical user interface Web browsers such as Mosaic and Netspace Navigators popularized the World Wide Web among end-users. Since then, we witnessed the booming of IT and its related fields and technologies, which eventually shifted the way that we live. In so many areas, it is really hard to imagine how we can live or work without Internet and the Web nowadays.

Besides the efforts and attentions from academics and industries and governments, we should never forget that it is a fact that end-users are also playing a curcial role in the development of Web. By understanding the importance of end-users, we embraced Web 2.0 which aims to get end-users more directly involved in the growth and improvement of Web.

When we look at the Web itself from a relatively high level, it essential a media that coordinates, allocates, and manipulates resources across Internet. For example, when searching on Google, what happening behind the scene is instead of searching on your own computers, Google's database is being searching to provide you candicate results, and all the coordinations are done on the Web. Thus, from the prespective of computer science or information techonology, Web itself is actually more like an opreting system.

When we use operating systems, either Windows or Mac OS X or Linux, we install software and software bundles like Microsoft Office to assist our daily works. Normally, if there is any software that cannot fulfill our requirements we can then just switch to alternative ones. For example, if you are not happy to use Microsoft Word to editing documents, you may want to use Apple's iWork. However, this kind of flexibility is missing in the field that we call "Web Services".

To date, almost eveyone of us who surf Internet are being benefited by Web services. For instance, you may use Starbucks app on your mobile phone to find where the nearest place to drink coffee. The app, or let's call it service, consists of Starbucks's own service that tells the address of each Starbucks sotre and, for example, Google Maps that can eventually show the locations to users. In this way, this service is more like a service bundle that we call "Composite Service". However, comparing with what we can do on traditional desktop operating systems, we can hardly make any changes to those primitive services involved in composite services to better serve our own requirements.

Once upon a time, those big companies such as Google, Microsoft, and Yahoo! have release their tools to assist end-users to create their own composite services. However, all of a sudden, most of these tools and projects are cancelled. Both academics and industries start to neglect the importance of end-users in the realm of Web services. They start even to forget the fact that without end-users, Internet and WWW would never archieve what they did nowadays.

Therefore, I, once again, propose that more attentions have to be paid to get more end-users involved in the development of Web services. Since we have already witnessed the success of Web 2.0, I would call the shift that I proposed "Web Service 2.0".

